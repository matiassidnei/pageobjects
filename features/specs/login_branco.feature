#language: pt

Funcionalidade: Realizar Login
     Como um usuário do site 
     Quero verificar se o usuário pode acessar os campos email e senha com dados inválidos

        @login_branco
        Cenário: Login em branco
            Dado que eu esteja na página de login do site
             Quando eu deixar em branco os campos de email e senha
              E clico no botao Sign in
             Então devo receber uma mensagem de alerta
     