class LoginPage <SitePrism::Page 
set_url 'index.php?controller=authentication'
element :cmp_email, '#email'
element :cmp_senha, '#passwd'
element :bt_sign_in, '#SubmitLogin'


    def preencher_login
        cmp_email.set 'matiassidnei@gmail.com' 
        cmp_senha.set  '123456'
    end

    def entrar
        bt_sign_in.click
    end
      
    def dados_invalidos
        cmp_email.set "     "
        cmp_senha.set "     "
    end
end
