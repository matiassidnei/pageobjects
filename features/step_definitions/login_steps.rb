Dado('que eu esteja na página de login do site') do
    @login_page = LoginPage.new
    @login_page.load    
end
  
  Quando('eu preencher os campos de email e senha com dados validos') do 
    @login_page.preencher_login
  end

  Quando('clico no botao Sign in') do
    @login_page.entrar
  end
  
  Então('devo ver a area logada do cliente') do
    expect(page).to have_text 'Welcome to your account. Here you can manage all of your personal information and orders.'
  end
  